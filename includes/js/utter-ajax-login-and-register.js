jQuery( document ).ready(function($) {
	"use strict";

	$( document ).on( 'click', '.utter_show_login', show_login )
			   .on( 'click', '.utter_logout', logout_user )
			   .on( 'click', '.utter_ajax_login_and_register_overlay', close_login )
			   .on( 'click', '.utter_register', register_user )
			   .on( 'click', '.utter_lost_password', lost_password )
			   .on( 'click', '.go_back_to_login', go_back_to_login )
			   .on( 'click', '.go_back_to_register', go_back_to_register )
			   .on( 'click', '#utter_login .submit_button', ajax_login_user )
			   .on( 'click', '#utter_register .submit_button', ajax_registration )
			   .on( 'click', '#utter_forgotten_pass .submit_button', ajax_lost_pass );

	function show_login(e){
		e.preventDefault();
		$( '.utter_ajax_login_and_register_overlay, #utter_login' ).fadeIn( 500 );
	}

	function close_login(e){
		$( '#utter_login, #utter_register, #utter_forgotten_pass, .utter_ajax_login_and_register_overlay' ).fadeOut( 500 );
		$( '.ajax_login .status' ).html( '' );
		$( '#utter_login' ).hide();
		$( '#utter_register, #utter_forgotten_pass' ).removeClass( 'show' );
	}

	function logout_user(e){
		e.preventDefault();
		window.location.href = ajax_login_object.logouturl;
	}

	// Perform AJAX login on form submit.
	function ajax_login_user(e){
		e.preventDefault();
		$.ajax({
			type: 'POST',
			url: ajax_login_object.ajaxurl,
			data: {
				'action': 'utter_ajax_login',
				'username': $( '#utter_login #username' ).val(),
				'password': $( '#utter_login #password' ).val(),
				'security': $( '#utter_login #security' ).val()
			},
			beforeSend: function(){
				if ($( '.error_message' ).length) {
					$( '.error_message' ).remove();
				}
				if ($( '#username' ).hasClass( 'error' )) {
					$( '#username' ).removeClass( 'error' );
				}
				if ($( '#password' ).hasClass( 'error' )) {
					$( '#password' ).removeClass( 'error' );
				}
				$( '#utter_login p.status' ).html( '' );
				$( '#utter_login p.status' ).show().text( ajax_login_object.loadingmessage );
			},
			success: function(data){
				console.log( data );
				$( '#utter_login p.status' ).text( data.message );
				if ( 'user_logged' === data ) {
					window.location.reload();
				} else {
					$( '#utter_login p.status' ).html( '' ).hide();
					var error_array = JSON.parse( data );
					for (var i = 0; i < error_array.length; i++) {
						var single_error = error_array[i];
						var error_split = error_array[i].split( '; ' );
						var error_msg = error_split[0];
						var $error_input = $( error_split[1] );
						if ( ! $error_input.hasClass( 'error' )) {
							$error_input.addClass( 'error' );
							$error_input.after( '<div class="error_message">' + error_msg + '</div>' );
						}
					}
				}
			},
			error : function (jqXHR, textStatus, errorThrown) {
				console.log( jqXHR + ' :: ' + textStatus + ' :: ' + errorThrown );
			}
		});
	}

	function register_user(e){
		e.preventDefault();
		if ( $( '.utter_ajax_login_and_register_overlay:hidden' ).length > 0 ) {
			$( '.utter_ajax_login_and_register_overlay' ).fadeIn( 500 );
		}
		$( '#utter_login' ).hide().addClass( 'hide' );
		$( '#utter_register' ).fadeIn( 500 ).addClass( 'show' );
	}

	// AJAX registration.
	function ajax_registration(e){
		e.preventDefault();

		$.ajax({
			type: 'POST',
			url: ajax_login_object.ajaxurl,
			data: {
				'action'           : 'utter_ajax_register',
				'user_login'       : $( '#user_login' ).val(),
				'user_email'       : $( '#user_email' ).val(),
				'terms'            : $( '#terms:checked' ).val(),
				'account_password' : $( '#account_password' ).val(),
				'user_register'    : $( '#user_register' ).val(),
			},
			beforeSend: function(){
				if ($( '.error_message' ).length) {
					$( '.error_message' ).remove();
				}
				if ($( '#user_login' ).hasClass( 'error' )) {
					$( '#user_login' ).removeClass( 'error' );
				}
				if ($( '#user_email' ).hasClass( 'error' )) {
					$( '#user_email' ).removeClass( 'error' );
				}
				if ($( '#account_password' ).hasClass( 'error' )) {
					$( '#account_password' ).removeClass( 'error' );
				}
				if ($( '#terms' ).hasClass( 'error' )) {
					$( '#terms' ).removeClass( 'error' );
				}
				$( '#utter_register p.status' ).html( '' );
				$( '#utter_register p.status' ).show().text( ajax_login_object.loadingmessage );
			},
			success: function(data) {
				if ( 'user_registered' === data ) {
					$( '#utter_register p.status' ).text( ajax_login_object.success_register );
					document.location.href = ajax_login_object.redirecturl;
				} else {
					$( '#utter_register p.status' ).html( '' ).hide();
					var error_array = JSON.parse( data );
					for (var i = 0; i < error_array.length; i++) {
						var single_error = error_array[i];
						var error_split = error_array[i].split( '; ' );
						var error_msg = error_split[0];
						var $error_input = $( error_split[1] );
						if ( ! $error_input.hasClass( 'error' ) ) {
							$error_input.addClass( 'error' );
							if ( 'terms' === $error_input.attr( 'id' ) ) {
								$error_input.next( 'label[for="terms"]' ).after( '<div class="error_message">' + error_msg + '</div>' );
							} else {
								$error_input.after( '<div class="error_message">' + error_msg + '</div>' );
							}
						}
					}
				}
			},
			error : function (jqXHR, textStatus, errorThrown) {
				console.log( jqXHR + ' :: ' + textStatus + ' :: ' + errorThrown );
			},
		});
	}

	function lost_password(e){
		e.preventDefault();
		$( '#utter_login' ).hide().addClass( 'hide' );
		$( '#utter_register' ).hide().addClass( 'hide' );
		$( '#utter_forgotten_pass' ).fadeIn( 500 ).addClass( 'show' );
	}

	// AJAX lost pass.
	function ajax_lost_pass(e){
		e.preventDefault();

		$.ajax({
			type: 'POST',
			url: ajax_login_object.ajaxurl,
			data: {
				'action'               : 'utter_lost_password',
				'user_forgotten_email' : $( '#user_forgotten_email' ).val(),
				'user_get_password'    : $( '#user_get_password' ).val(),
			},
			beforeSend: function(){
				if ($( '.error_message' ).length) {
					$( '.error_message' ).remove();
				}
				if ($( '#user_forgotten_email' ).hasClass( 'error' )) {
					$( '#user_forgotten_email' ).removeClass( 'error' );
				}
				$( '#utter_forgotten_pass p.status' ).html( '' );
				$( '#utter_forgotten_pass p.status' ).show().text( ajax_login_object.loadingmessage );
			},
			success: function(data) {
				if ( 'mail_sent' === data ) {
					$( '#utter_forgotten_pass p.status' ).text( ajax_login_object.mail_sent );
					window.location.reload();
				} else {
					$( '#utter_forgotten_pass p.status' ).html( '' ).hide();
					var error_array = JSON.parse( data );
					for (var i = 0; i < error_array.length; i++) {
						var single_error = error_array[i];
						var error_split = error_array[i].split( '; ' );
						var error_msg = error_split[0];
						var $error_input = $( error_split[1] );
						if ( ! $error_input.hasClass( 'error' ) ) {
							$error_input.addClass( 'error' ).after( '<div class="error_message">' + error_msg + '</div>' );
						}
					}
				}
			},
			error : function (jqXHR, textStatus, errorThrown) {
				console.log( jqXHR + ' :: ' + textStatus + ' :: ' + errorThrown );
			},
		});
	}

	function go_back_to_login(e){
		e.preventDefault();
		$( '#utter_login' ).removeClass( 'hide' ).show();
		$( '#utter_register' ).removeClass( 'show' ).hide();
		$( '#utter_forgotten_pass' ).removeClass( 'show' ).hide();
	}

	function go_back_to_register(e){
		e.preventDefault();
		$( '#utter_register' ).removeClass( 'hide' ).addClass( 'show' ).show();
		$( '#utter_forgotten_pass' ).removeClass( 'show' ).hide();
		$( '#utter_login' ).removeClass( 'show' ).hide();
	}

});
