<?php
/**
 * Login menu item in nav menu meta box
 *
 * @link       https://utterwp.com/
 * @since      1.0.0
 *
 * @package    Utter_Ajax_Login_And_Register
 * @subpackage Utter_Ajax_Login_And_Register/includes
 */

/**
 * Login menu metabox class
 *
 * Will add login meta box in nav menu page.
 *
 * @package    Utter_Ajax_Login_And_Register
 * @subpackage Utter_Ajax_Login_And_Register/includes
 * @author     Utterwp <info@utterwp.com>
 */
class Utter_Login_Menu_Metabox {
	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $utter_ajax_login_and_register    The ID of this plugin.
	 */
	private $utter_ajax_login_and_register;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param  string $utter_ajax_login_and_register       The name of this plugin.
	 * @param  string $version    The version of this plugin.
	 */
	public function __construct( $utter_ajax_login_and_register, $version ) {

		$this->utter_ajax_login_and_register = $utter_ajax_login_and_register;
		$this->version = $version;

	}

	/**
	 * Add nav menu meta box with login menu item.
	 *
	 * @since 1.0.0
	 */
	public function utter_add_nav_menu_meta_box() {
		add_meta_box(
			'utter_login_menu_add',
			esc_html__( 'Add Login Menu Item', 'utter-ajax-login-and-register' ),
			array( $this, 'utter_login_nav_menu_link' ),
			'nav-menus',
			'side',
			'low'
		);
	}

	/**
	 * Adds content to the sidebar login
	 *
	 * @since 1.0.0
	 */
	public function utter_login_nav_menu_link() {
		?>
		<div id="login" class="posttypediv">
			<div id="tabs-panel-login" class="tabs-panel tabs-panel-active">
				<ul id ="login-checklist" class="categorychecklist form-no-clear">
					<li>
						<label class="menu-item-title">
							<input type="checkbox" class="menu-item-checkbox" name="menu-item[-1][menu-item-object-id]" value="menu_login"> <?php esc_html_e( 'Login', 'utter-ajax-login-and-register' ); ?>
						</label>
						<input type="hidden" class="menu-item-type" name="menu-item[-1][menu-item-type]" value="custom">
						<input type="hidden" class="menu-item-attr-title" name="menu-item[-1][menu-item-attr-title]" value="<?php esc_html_e( 'Login', 'utter-ajax-login-and-register' ); ?>">
						<input type="hidden" class="menu-item-title" name="menu-item[-1][menu-item-title]" value="<?php esc_html_e( 'Login', 'utter-ajax-login-and-register' ); ?>" />
						<input type="hidden" class="menu-item-url" name="menu-item[-1][menu-item-url]" value="#">
						<input type="hidden" class="menu-item-classes" name="menu-item[-1][menu-item-classes]" value="utter_show_login">
						<input type="hidden" class="menu-item-description" name="menu-item[-1][menu-item-description]" value="">
					</li>
				</ul>
			</div>
			<p class="button-controls">
				<span class="list-controls">
					<a href="<?php echo esc_url( admin_url( 'nav-menus.php?page-tab=all&amp;selectall=1#login' ) ); ?>" class="select-all"><?php esc_html_e( 'Select All', 'utter-ajax-login-and-register' ); ?></a>
				</span>
				<span class="add-to-menu">
					<input type="submit" class="button-secondary submit-add-to-menu right" value="<?php esc_html_e( 'Add to Menu', 'utter-ajax-login-and-register' ); ?>" name="add-login-menu-item" id="submit-login">
					<span class="spinner"></span>
				</span>
			</p>
		</div>
	<?php
	}

}
